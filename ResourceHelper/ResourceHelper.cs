using System;
using System.IO;
using System.Reflection;

namespace ResourceHelper
{
    public static class ResourceHelper
    {
        public static string GetString(string name, object sibling)
        {
            using (var stream = GetStream(name, sibling))
            {
                return new StreamReader(stream).ReadToEnd();
            }
        }

        private static Stream GetStream(string name, object sibling)
        {
            var assembly = sibling.GetType().Assembly;
            name = sibling.GetType().Namespace + "." + name;
            var stream = assembly.GetManifestResourceStream(name);
            if (stream == null)
            {
                throw new ApplicationException("Could not find stream named " + name + " in " + assembly.FullName);
            }
            return stream;
        }

        public static byte[] GetBytes(string name, object sibling)
        {
            using (var stream = GetStream(name, sibling))
            {
                var result = new byte[stream.Length];
                stream.Read(result, 0, result.Length);
                return result;
            }
        }
    }
}
