using System;
using System.Reflection;
using System.Text;
using NUnit.Framework;

namespace ResourceHelper.Tests
{
    public class TestResourceHelper
    {
        private const string _SAMPLE1 = "sample1.txt";
        private const string _SAMPLE1_CONTENT = "Hello world";

        [Test]
        public void ItCanGetAstring()
        {
            var content = ResourceHelper.GetString(_SAMPLE1, this);
            Assert.That(content, Is.EqualTo(_SAMPLE1_CONTENT));
        }

        [Test]
        public void ItCanGetBytes()
        {
            var content = ResourceHelper.GetBytes(_SAMPLE1, this);
            Assert.That(content, Is.EqualTo(Encoding.UTF8.GetBytes(_SAMPLE1_CONTENT)));
        }
    }
}
